import {ApplicationService, HttpError} from "@themost/common";
import unirest from 'unirest';
const os = require('os');

class SmsCanalService extends ApplicationService {
    constructor(app) {
        super(app);

        this.settings = Object.assign({
                sender : null, // set default sender name
                url: null, // set default SMSCanal url
                user: null, // set default SMSCanal user
                password: null, // set default SMSCanal user password
                letterCase: null, // set default SMSCanal user password
            }, app.getConfiguration().getSourceAt('settings/sms-canal'));
    }

    async sendSMS(context, message) {
        let recipient = message.recipient;
        const smsRecipient = await context.model('CandidateStudent')
            .where('user').equal(recipient)
            .select('person/mobilePhone as phone').silent()
            .getItem() ?? await context.model('Student')
            .where('user').equal(recipient)
            .select('person/mobilePhone as phone').silent()
            .getItem() ?? await context.model('Instructor')
            .where('user').equal(recipient)
            .select('workPhone as phone').silent()
            .getItem();

        if (smsRecipient == null) {
            throw new Error('Message recipient cannot be found or is inaccessible');
        }
        if (smsRecipient.phone == null) {
            throw new Error('Recipient phone cannot be found or is inaccessible');
        }

        return this.sendDirectSMS(smsRecipient.phone, message.body);
    }

    sendDirectSMS(recipient, message) {
        let msg = '';
        let mobiles = [];
        let strMobiles = '';

        if (message.length > 0 ) {
            if (this.settings.letterCase && this.settings.letterCase === 'uppercase') {
                msg = this.normalizeGreek(message).toUpperCase();
            } else {
                msg = this.normalizeGreek(message);
            }
        } else {
            return false;
        }
        if (recipient.trim().indexOf(',') > 0) {
            mobiles = recipient.split(',');
            mobiles = mobiles.filter( m=> this.phoneEuropeRegExp(m.trim()));
        } else {
            if (this.phoneEuropeRegExp(recipient.trim())) {
                mobiles.push(recipient.trim());
            }
        }

        strMobiles = mobiles.join(', ');

        let requestBody = {
            'user': this.settings.user,
            'password': this.settings.password,
            'mobiles': strMobiles,
            'senderid': this.settings.sender,
            'sms': msg
        }

        return new Promise( (resolve, reject) => {
            if(mobiles.length === 0) {
                return reject(false);
            }

            return unirest.post(this.settings.url)
                .header('Cache-Control', 'no-cache')
                .header('Accept', 'application/json')
                .send(requestBody)
                .end(function (response) {
                    const body =  response.body;
                    if (body && body.error) {
                        return reject(Object.assign(new HttpError(body.code || 500), body));
                    }
                    return resolve(true);
                });
        });
    }

    normalizeGreek(text) {
        text = text.replace(/Ά|Α|ά/g, 'Α')
            .replace(/Έ|Ε|έ/g, 'Ε')
            .replace(/Ή|Η|ή/g, 'Η')
            .replace(/Ί|Ϊ|Ι|ί|ΐ|ϊ/g, 'Ι')
            .replace(/Ό|Ο|ό/g, 'Ο')
            .replace(/Ύ|Ϋ|Υ|ύ|ΰ|ϋ/g, 'Υ')
            .replace(/Ώ|Ω|ώ/g, 'Ω')
            .replace(/Σ|ς/g, 'Σ');
        return text;
    }

    phoneEuropeRegExp(phone) {
        // eu phones map https://en.wikipedia.org/wiki/Telephone_numbers_in_Europe
        const phonesMap = {
            "43": [4, 13],
            "32": [8, 10],
            "359": [7, 9],
            "385": [8, 9],
            "357": 8,
            "420": 9,
            "45": 8,
            "372": 7,
            "358": [5, 12],
            "33": 9,
            "350": 8,
            "49": [3, 12],
            "30": 10,
            "36": [8, 9],
            "354": [7, 9],
            "353": [7, 9],
            "39": [6, 12],
            "371": 8,
            "423": [7, 12],
            "370": 8,
            "352": 8,
            "356": 8,
            "31": 9,
            "47": [4, 12],
            "48": 9,
            "351": 9,
            "40": 9,
            "421": 9,
            "386": 8,
            "34": 9,
            "46": [6, 9],
        };
        const regExpBuilt = Object.keys(phonesMap)
            .reduce(function(prev, key) {
                const val = phonesMap[key];
                if (Array.isArray(val)) {
                    prev.push("(\\+?" + key + `[0-9]\{${val[0]},${val[1]}\})`);
                } else {
                    prev.push("(\\+?" + key + `[0-9]\{${val}\})`);
                }
                return prev;
            }, [])
            .join("|");
        const regExp = new RegExp(`^(${regExpBuilt})$`);
        return regExp.test(phone);
    }

    async checkBalance() {
        throw new Error('Not implemented');
    }

    async checkDelivery(message) {
        throw new Error('Not implemented');
    }

}
export {SmsCanalService}
