import {SmsCanalService} from '../src/SmsCanalService';
import app from '../../../server/app'
describe('LibraryService', () => {

    // it('should create instance', async () => {
    //     const app1 = app.get('ExpressDataApplication');
    //     //const service = new SmsCanalService();
    //     expect(app1).toBeTruthy();
    // });

    it('should create instance', async () => {
        const app1 = app.get('ExpressDataApplication');
        const service = new SmsCanalService(app1);
        const result = await service.sendDirectSMS('6980123456, +6980123457, 6981234','Η ΕΓΓΡΑΦΗ ΣΑΣ ΣΤΟ Δ.Π.Θ. ΟΛΟΚΛΗΡΩΘΗΚΕ ΕΠΙΤΥΧΩΣ username : apolitis password: xXyYzZkKoO ΟΡΟΙ ΧΡΗΣΗΣ, ΠΛΗΡΟΦΟΡΙΕΣ: https://welcome.duth.gr');
        expect(app1).toBeTruthy();
        expect(service).toBeTruthy();
        expect(result).toBeTruthy();
    });


});
